Hello, this is my AR-VR Application that build with Vuforia

This application core feature is switch from AR mode to VR mode
Please take this note before you install and use it
1. Make sure your device is Android 4.1 or later
2. Have at least ram 4GB to run smoothly

Here is some screenshot from this application

![picture](ar-vr-base-framework-master/image/Screenshot_20200324-201701_AR-VR Debug.jpg)
![picture](ar-vr-base-framework-master/image/Screenshot_20200324-201728_AR-VR Debug.jpg)

If you have some question, don't hesitate to contact me

Thank you!